/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */

import util from "util";
import fs from "fs";

import { test } from "uvu";
import * as assert from "uvu/assert";
import * as dotenv from "dotenv";
import SMTPTransport from "nodemailer/lib/smtp-transport/index.js";

import Trogon from "../src/Trogon";

dotenv.config({ path: "test/.env" });
const transportOptions = {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    auth: {
        user: process.env.SMTP_AUTH_USER,
        pass: process.env.SMTP_AUTH_PASS,
    },
} as SMTPTransport.Options;


// Define a valid configuration object
const validConfig = {
    inputHtmlPath: "./test/test-in.min.html",
    inputCssPath: "./test/test-in.css",
    outputHtmlPath: "./test/test-out.html",
    outputEmlPath: "./test/test-out.eml",
    outputLogsPath: "./test/test-logs.json",
};

declare global {
    interface String {
        cleanse(): string;
    }
}
String.prototype.cleanse = function () {
    return this.replace(/\s/g, "");
}

const htmlIn = `<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]--><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title></title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" href=""></head><body><!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
<![endif]--><!-- this is a comment --><script src=""></script><!--[if !mso]>
<div class="mso"></div>
<![endif]--><div class="bloppers"></div></body></html>`;
const htmlInWithImg = `<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title></title><meta name="description" content=""><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href=""></head><body>    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Panor%C3%A1mica_Oto%C3%B1o_Alc%C3%A1zar_de_Segovia.jpg/1280px-Panor%C3%A1mica_Oto%C3%B1o_Alc%C3%A1zar_de_Segovia.jpg"></img></body></html>`;
const htmlInWithTitle = `<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Crickets</title><meta name="description" content=""><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href=""></head><body></body></html>`;

const subject = "Crickets";
const domain = "quorum.ltd";
const sender = "sender@quorum.ltd";
const recipients = ["recipient1@quorum.ltd", "recipient2@quorum.ltd"];

test("Trogon initialises with valid config and returns a proxy", () => {
    const instance = new Trogon(validConfig);
    assert.ok(util.types.isProxy(instance));
});

test("Trogon.loadHtmlFromString() reads an HTML file and stores into this.state", async () => {
    const trogonState = await new Trogon(validConfig)
        .loadHtmlFromFile()
        .state;

    const actual = trogonState.$!.html().cleanse();
    const expected = htmlIn.cleanse();

    assert.is(actual, expected);
});

test("Trogon.loadHtmlFromString() reads an HTML from a string and stores into this.state", async () => {
    const trogonState = await new Trogon(validConfig)
        .loadHtmlFromString(htmlIn)
        .state;

    const actual = trogonState.$!.html();
    const expected = htmlIn;

    assert.is(actual, expected);
});

test("Trogon.cleanComments() removes all comment nodes", async () => {
    const trogonState = await new Trogon(validConfig)
        .loadHtmlFromString(htmlIn)
        .cleanComments()
        .state;

    const actual = trogonState.$!.html();
    const expected = `<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]--><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title></title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" href=""></head><body><script src=""></script><div class="bloppers"></div></body></html>`;

    assert.is(actual, expected);
});

test("Trogon.cleanMsoConditions() removes MSO specific conditionals", async () => {
    const trogonState = await new Trogon(validConfig)
        .loadHtmlFromString(htmlIn)
        .cleanMsoConditions()
        .state;

    const actual = trogonState.$!.html().cleanse();
    const expected = `<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]--><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title></title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" href=""></head><body><!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]--><!-- this is a comment --><script src=""></script><div class="bloppers"></div></body></html>`
        .cleanse();

    assert.is(actual, expected);
});

test("Trogon.cleanConditionalComments() removes all conditionals", async () => {
    const trogonState = await new Trogon(validConfig)
        .loadHtmlFromString(htmlIn)
        .cleanConditionalComments()
        .state;

    const actual = trogonState.$!.html().cleanse();
    const expected = `<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title></title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" href=""></head><body><!-- this is a comment --><script src=""></script><div class="bloppers"></div></body></html>`
        .cleanse();

    assert.is(actual, expected);
});

test("Trogon.dumpHtml() writes the current HTML state to a file", async () => {
    const trogonWriterState = await new Trogon(validConfig)
        .loadHtmlFromString(htmlIn)
        .dumpHtml()
        .state;

    const trogonReaderState = await new Trogon({
        ...validConfig,
        inputHtmlPath: validConfig.outputHtmlPath
    })
        .loadHtmlFromFile()
        .state;

    const actual = trogonReaderState.$!.html().cleanse();
    const expected = trogonWriterState.$!.html().cleanse();

    assert.is(actual, expected);
});

test("Trogon.dumpLogsToFile() writes logs stream to a file", async () => {
    const trogonState = await new Trogon(validConfig)
        .dumpLogsToFile()
        .state;

    const actual = JSON.parse(fs.readFileSync(validConfig.outputLogsPath).toString())[0].msg;
    const expected = JSON.parse(JSON.stringify(trogonState.logCacheStream.logCache[0])).msg;

    assert.is(actual, expected);
});

test(
    "Trogon.embedExternalImgAsBase64() converts and embeds all external image sources within the HTML document to Base64-encoded data URIs.",
    async () => {
        await new Trogon(validConfig)
            .loadHtmlFromString(htmlInWithImg)
            .embedExternalImgAsBase64()
            .dumpHtml();

        const actualLen = fs.readFileSync(validConfig.outputHtmlPath).toString().cleanse().length;
        const baseLen = htmlInWithImg.cleanse().length;

        assert.ok(actualLen > baseLen);
    }
);

test(
    "Trogon.inferSubjectFromTitle() extracts the text content of the HTML <title> tag to use as the email subject.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlInWithTitle)
            .inferSubjectFromTitle()
            .state;

        const expected = "Crickets";
        const actual = trogonState.subject;

        assert.is(actual, expected);
    }
);

test(
    "Trogon.setSubject() assigns a user-defined subject to the instance's state for use in email construction.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .setSubject(subject)
            .state;

        const expected = subject;
        const actual = trogonState.subject;

        assert.is(actual, expected);
    }
);

test(
    "Trogon.composeMail() prepares the email object with the subject and HTML body.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .state;

        const actual = trogonState.mail;

        assert.ok(actual);
    }
);

test(
    "Trogon.markAsUnsent() sets the X-Unsent header to 1.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .markAsUnsent()
            .state;

        const headers = trogonState.mail!.headers as any;
        assert.ok(headers);
        const actual = headers["X-Unsent"];
        assert.ok(actual);
    }
);

test(
    "Trogon.setDomain() assigns a user-defined domain name to be used for Message-ID generation.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .setDomain(domain)
            .state;

        const actual = trogonState.domain;
        assert.ok(actual);
    }
);

test(
    "Trogon.generateMessageId() generates a Message-ID header for the mail message using specified domain.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setDomain(domain)
            .generateMessageId()
            .state;

        const actual = trogonState.mail!.messageId;
        assert.ok(actual);
    }
);

test(
    "Trogon.dumpEml() generates a standard MIME structure (EML file) from the composed mail object.",
    async () => {
        await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .cleanComments()
            .cleanConditionalComments()
            .composeMail()
            .dumpEml()

        const actual = fs.readFileSync(validConfig.outputEmlPath).toString();

        assert.match(actual, /MIME-Version: 1.0/gm);
    }
);

test(
    "Trogon.cleanMessageId() removes the Message-ID header from the output EML file.",
    async () => {
        await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .cleanComments()
            .cleanConditionalComments()
            .composeMail()
            .dumpEml()
            .cleanMessageId();

        const actual = fs.readFileSync(validConfig.outputEmlPath).toString();

        assert.not.match(actual, /Message-ID: /gm);
    }
);

test(
    "Trogon.setSmtpCred() configures the Nodemailer SMTP transport.",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .setSmtpCred(transportOptions)
            .state;
        
        const actual = trogonState.smtpTransport;

        assert.ok(actual);
    }
);

test(
    "Trogon.setSender() sets the sender for the composed mail object",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setSender(sender)
            .state;

        const actual = trogonState.mail!.from;
        const expected = sender;

        assert.equal(actual, expected);
    }
);

test(
    "Trogon.setSmtpCred() additionally sets mail.from if and only if it's empty.",
    async () => {
        const trogonStateEmptyFrom = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setSmtpCred(transportOptions)
            .state;
        
        const actual1 = trogonStateEmptyFrom.mail!.from;
        const expected = transportOptions.auth!.user;

        assert.equal(actual1, expected);

        const trogonStateExplicitFrom = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setSender(sender)
            .setSmtpCred(transportOptions)
            .state;

        const actual2 = trogonStateExplicitFrom.mail!.from;

        assert.not.equal(actual2, expected);
    }
);

test(
    "Trogon.setRecipients() sets recipients array for the composed mail object",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setRecipients(recipients)
            .state;

        const actual = trogonState.mail!.to;
        const expected = recipients;

        assert.equal(actual, expected);
    }
);

test(
    "Trogon.setRecipients() also sets a single string recipient for the composed mail object",
    async () => {
        const trogonState = await new Trogon(validConfig)
            .loadHtmlFromString(htmlIn)
            .composeMail()
            .setRecipients(recipients[0])
            .state;

        const actual = trogonState.mail!.to;
        const expected = recipients[0];

        assert.equal(actual, expected);
    }
);

// TODO: sendEmail test with nodemailer mock
// TODO: css methods tests
// TODO: some tests for some error throws

test.run();
