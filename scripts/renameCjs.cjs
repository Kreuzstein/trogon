/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */

/*
 * renameCjs.js
 *
 * This is a post-processing script for renaming .js file extensions in
 * dist/cjs to .cjs so that CommonJS imports (via `require`) are supported.
 */

const fs = require("fs");
const path = require("path");

// Specify the directory where your .js files are located
const DIRECTORY = path.join(__dirname, "../dist/cjs");

const processDirectory = (directoryPath) => {
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            return console.error("Unable to scan directory:", err);
        }

        files.forEach((file) => {
            if (file.endsWith(".js")) {
                const filePath = path.join(directoryPath, file);
                const newFilePath = filePath.replace(".js", ".cjs");

                fs.rename(filePath, newFilePath, (err) => {
                    if (err) throw err;
                    console.log(`${file} was renamed to ${newFilePath}`);
                });
            }
        });
    });
};

processDirectory(DIRECTORY);
