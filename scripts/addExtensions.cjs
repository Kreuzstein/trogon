/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */

/**
 * addExtensions.js
 *
 * This is a post-processing script for adding .js extensions
 * to certain ES module imports (it's required in JS but not
 * TS files).
 */

const fs = require("fs");
const path = require("path");

// Specify the directory where your .js files are located
const DIRECTORY = path.join(__dirname, "../dist");

// Regular expression to match import/export statements
const importExportRegex = /((?:(?:import|export).*?from\s+['"])|(?:require\(['"]))(.*?)(['"])/gm;

// Exceptions that are non-relative imports that still need an extension
const exceptions = [
    "nodemailer/lib/mail-composer",
    "nodemailer/lib/mailer",
    "nodemailer/lib/smtp-transport",
];

// Function to process each file and add the .js extension to import/export statements
const processFile = (filePath, extension = ".js") => {
    fs.readFile(filePath, "utf8", (err, data) => {
        if (err) {
            console.error(`Error reading file: ${filePath}`, err);
            return;
        }

        const updatedData = data.replace(
            importExportRegex,
            (match, importStatement, module, endQuote) => {
                // Handle exceptions
                if (exceptions.includes(module)) {
                    return `${importStatement}${module}/index.js${endQuote}`;
                }
                // Skip module specifiers with no extension and non-relative paths
                if (!module.startsWith(".") || path.extname(module)) {
                    return match;
                }

                return `${importStatement}${module}${extension}${endQuote}`;
            }
        );

        fs.writeFile(filePath, updatedData, "utf8", (err) => {
            if (err) {
                console.error(`Error writing file: ${filePath}`, err);
            } else {
                console.log(`Updated file: ${filePath}`);
            }
        });
    });
};

// Function to recursively process all .js files in a directory
const processDirectory = (directory) => {
    fs.readdir(directory, { withFileTypes: true }, (err, entries) => {
        if (err) {
            console.error(`Error reading directory: ${directory}`, err);
            return;
        }

        entries.forEach((entry) => {
            const fullPath = path.join(directory, entry.name);
            if (entry.isDirectory()) {
                processDirectory(fullPath);
            } 
            if (!entry.isFile()) return;
            if (path.extname(entry.name).toLowerCase() === ".js" ) {
                processFile(fullPath);
            }
            if (path.extname(entry.name).toLowerCase() === ".cjs" ) {
                processFile(fullPath, ".cjs");
            }
        });
    });
};

// Start processing the directory
processDirectory(DIRECTORY);
