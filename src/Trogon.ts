/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */
import * as fs from "fs";
import * as path from "path";
import { randomUUID } from "crypto";

import errorCodes from "./errorCodes";
import TrogonError from "./TrogonError";
import TrogonLogCacheStream from "./TrogonLogCacheStream";

import axios from "axios";
import { type CheerioAPI, load as loadWithCheerio, AnyNode } from "cheerio";
import { Transporter, createTransport } from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import MailComposer from "nodemailer/lib/mail-composer";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import juice from "juice";
import bunyan from "bunyan";

const errorModes = { throw: "throw", log: "log" } as const;
/** Configuration options for the Trogon instance. */
export type Config = {
    /** Path to the input HTML file. Optional. */
    inputHtmlPath?: string,
    /** Path to the input CSS file. Optional. */
    inputCssPath?: string,
    /** Path where the processed HTML will be saved. Defaults to `./out.html`. */
    outputHtmlPath?: string,
    /** Path where the processed EML file will be saved. Defaults to `./out.eml`. */
    outputEmlPath?: string,
    /** Path where logs will be saved, if logging is enabled. Optional. */
    outputLogsPath?: string,
    /** Mode to handle errors. Can be 'throw' or 'log'. Defaults to `throw`. */
    errorMode?: typeof errorModes[keyof typeof errorModes],
    /** The maximum time (in milliseconds) before timing out. Defaults to 30000 ms. */
    timeout?: number,
};

export type State = {
    /** An instance of a `bunyan` logger used for logging information, warnings and errors. */
    logger: bunyan,
    /** A custom stream object compatible with `bunyan` used for caching or storing logs. */
    logCacheStream: TrogonLogCacheStream,
    /** The subject line to be used in the email. Defaults to "No Subject". */
    subject: string,
    /** An instance of the Cheerio API that allows for traversing and manipulating the HTML content. Optional. */
    $?: CheerioAPI,
    /** An object describing the email configuration options, based on the `nodemailer` `Mail.Options` type. Optional. */
    mail?: Mail.Options,
    /** The domain to use when constructing Message-ID. Optional. */
    domain?: string,
    /** An instance of a `nodemailer` SMTP transporter for sending emails. This transporter includes configuration for the SMTP server and is used to dispatch emails. Optional. */
    smtpTransport?: Transporter<SMTPTransport.SentMessageInfo>,
}

const logsDict = {
    created: "Trogon instance created.",
} as const;

/**
 * Trogon is a util that prepares HTML content for email distribution. Its primary function is to generate
 * MIME/EML files suitable for a wide range of email clients, with the focus on compatibility in older 
 * email clients, particularly desktop versions of Microsoft Outlook.
 *
 * Trogon provides a series of methods for sanitising, inlining styles, and manipulating the HTML and email data,
 * aimed at minimizing common rendering issues encountered when emails are viewed in less modern email applications.
 *
 * Users have the flexibility to directly access and manipulate the `state` of an active Trogon instance at any
 * stage of the email preparation workflow, providing a means to perform custom transformations outside of the
 * provided utility functions.
 *
 * Built atop well-established Node.js libraries such as Cheerio for HTML manipulation, Nodemailer for email 
 * generation and sending, and Juice for inlining CSS, Trogon integrates these tools to offer a cohesive and 
 * reliable solution for email content preparation without unexpected complexities.
 */
class Trogon {
    [key: string]: State | Config | Function | Mail.Options | undefined;

    private static defaultState: Omit<State, "logger" | "logCacheStream"> = {
        $: undefined,
        subject: "No Subject",
        mail: undefined,
    };

    /**
     * Retrieves an image from the specified URL and encodes it as a Base64 string.
     * 
     * This method fetches the image data as an array buffer using an HTTP GET request
     * and then converts the buffer into a Base64 encoded string. The Base64 encoded
     * image string can be used for embedding the image directly into web pages,
     * stylesheets, or other media that accept Base64 encoded data URIs.
     * 
     * @param url The URL of the image to be fetched and encoded.
     * @returns A promise that resolves with the Base64 encoded string of the image data.
     */
    private static async getImageBase64(url: string, timeout: number = 30000) {
        const response = await axios.get(url, {
            responseType: "arraybuffer",
            timeout,
        });
        return Buffer.from(response.data).toString("base64");
    }

    private config: Config;

    /**
     * Creates a dynamic Proxy for the current instance to enable method chaining and
     * centralised asynchronous handling.
     * 
     * This method creates a Proxy where method calls return promises for the
     * original methods, allowing chained calls to wait for the completion of the
     * previous one. It also enables the logging of method calls and error handling
     * through a defined `handleError` method. Non-function properties are accessed
     * normally via `Reflect.get`.
     * 
     * @param promise A promise wrapping the current instance, defaulting to a resolved promise of 'this'.
     * @returns A Proxy of the current instance which intercepts method calls for chaining purposes.
     * 
     * Method calls on the Proxy, if they are functions, get logged and then invoked. If the invoked
     * methods return a promise, they can be chained together while maintaining a sequence. The returned 
     * promise from each method call is wrapped inside a new proxy to facilitate uninterrupted chaining.
     * Errors from method invocations are captured and passed to a centralised `handleError` method.
     * 
     * If a property is not a function, it is retrieved as-is without proxying or additional logic.
     */
    private createProxy(promise = Promise.resolve(this)) {
        const instance = this;
        return new Proxy(() => { }, {
            get(_target, prop: string, receiver) {
                if (prop === "state") {
                    instance.state.logger.info(`Getting Trogon.state`);
                    return promise.then(() => instance.state);
                }
                if (typeof instance[prop] === "function") {
                    return (...args: any[]) => {
                        const nextPromise = promise.then(() => {
                            // Log what method was called
                            instance.state.logger.info(`Called Trogon.${prop}(${JSON.stringify(args).slice(1, -1)})`);

                            return (instance[prop] as Function).apply(instance, args);
                        }).catch((err: any) => {
                            instance.handleError(err);
                        });

                        // Return a proxy that wraps the next promise,
                        // whether the previous operation succeeded or failed
                        return instance.createProxy(nextPromise);
                    };
                }
                instance.state.logger.info(`Called Trogon.${prop}`);
                return Reflect.get(instance, prop, receiver);
            }
        })
    }

    /**
     * Handles errors that occur during execution of the Trogon instance's methods.
     *
     * This method serves as a centralised error handling mechanism for the Trogon class.
     * It logs the error message using the configured logger and determines the course
     * of action based on the error handling configuration provided in `this.config`.
     *
     * If the configured error mode is set to 'throw', the error is re-thrown to be
     * handled by the caller or to propagate up the call stack. For other error modes,
     * the method will simply log the error without interrupting execution flow.
     *
     * @param err The Error object that was caught and needs to be handled.
     */
    private handleError(err: Error) {
        this.state.logger.error(err.message);
        if (this.config.errorMode === errorModes.throw) {
            throw err;
        }
    }

    /**
     * Loads the CSS content from the file system based on the configured file path.
     *
     * This method reads the contents of a CSS file whose path is specified in the
     * configuration object of the Trogon instance (`this.config`). It is designed to
     * synchronously read and return the entire CSS stylesheet as a string.
     *
     * @returns A string containing the content of the CSS file.
     * @throws {Error} If `inputCssPath` is missing, an error is thrown with a code
     * indicating that no CSS file was found.
     */
    private loadCss() {
        const { inputCssPath } = this.config;
        if (!inputCssPath) throw new Error(errorCodes.noCssFileFound);
        const styleSheet = fs.readFileSync(inputCssPath, 'utf8');
        return styleSheet;
    }

    /**
     * Current state of the Trogon instance.
     */
    state: State;

    /**
     * Creates a new `Trogon` instance with the specified configuration.
     * 
     * @typedef {import("../types/Trogon.js").Config} Config
     * @returns {this} The current `Trogon` instance
     */
    constructor(config: Config = {}) {
        this.config = config;
        if (!this.config.outputEmlPath) this.config.outputEmlPath = "./out.eml";
        if (!this.config.outputHtmlPath) this.config.outputHtmlPath = "./out.html";
        if (!this.config.timeout) this.config.timeout = 30000;
        if (!this.config.errorMode) this.config.errorMode = errorModes.throw;
        const logCacheStream = new TrogonLogCacheStream();
        const logger = bunyan.createLogger({
            name: "Trogon",
            streams: [{
                stream: logCacheStream,
                type: "raw",
                level: "trace",
            },
            {
                stream: process.stdout,
                level: "error",
            }]
        });
        logger.trace(logsDict.created);
        this.state = {
            ...Trogon.defaultState,
            logger,
            logCacheStream,
        };
        return this.createProxy() as unknown as Trogon;
    }

    /**
     * Loads HTML content from a file into the instance's state using Cheerio.
     *
     * This method synchronously reads HTML content from a file path specified in the
     * `inputHtmlPath` property of the instance's configuration object. It then
     * initialises Cheerio with the loaded HTML content, allowing for further DOM
     * manipulation or querying using Cheerio's jQuery-like API. 
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    loadHtmlFromFile() {
        const { inputHtmlPath } = this.config;
        if (!inputHtmlPath) throw new Error(errorCodes.noHtmlFileFound);
        const htmlContent = fs.readFileSync(inputHtmlPath);
        this.state.$ = loadWithCheerio(htmlContent);
        return void 0 as unknown as this;
    }

    /**
     * Parses HTML content from a string into the instance's state using Cheerio.
     *
     * This method initialises Cheerio with the loaded HTML content, allowing for further DOM
     * manipulation or querying using Cheerio's jQuery-like API. 
     *
     * @param {string} htmlContent String containing the HTML content.
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    loadHtmlFromString(htmlContent: string) {
        this.state.$ = loadWithCheerio(htmlContent);
        return void 0 as unknown as this;
    }

    /**
     * Saves the current state of the HTML document to a file.
     *
     * This method extracts the HTML content from the Cheerio instance stored in the
     * class state and writes it to an output file specified by the `outputHtmlPath`
     * property in the class configuration object. 
     * 
     * The method employs synchronous file writing to store the HTML content in the
     * specified file using UTF-8 encoding. 
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    dumpHtml() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const html = $.html();
        fs.writeFileSync(
            this.config.outputHtmlPath!,
            html,
            "utf8");
        return void 0 as unknown as this;
    }

    /**
     * Writes the cached log messages to the file configured in `outputLogsPath`.
     *
     * This method is responsible for persisting the log messages collected in the
     * instance's `logCacheStream` to a physical file. It uses the path provided by
     * the `outputLogsPath` property from the instance's configuration object as the
     * destination for the log output file. 
     *
     * The actual writing of the logs to the file is delegated to the `dumpCacheToFile`
     * method of the `LogCacheStream` object stored in `this.state`. 
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `outputLogsPath` is not set, an error with a predefined code is thrown.
     */
    dumpLogsToFile() {
        const { outputLogsPath } = this.config;
        if (!outputLogsPath) throw new Error(errorCodes.noSetLogsFile);

        this.state.logCacheStream.dumpCacheToFile(outputLogsPath);
        return void 0 as unknown as this;
    }

    /**
     * Outputs the current cache of log messages to the console for inspection.
     *
     * This method is designed to display the contents of the log messages that have
     * been cached in memory by the `LogCacheStream` instance. The full array of
     * cached log message objects is logged to the console using `console.info`.
     * This functionality is typically used for debugging purposes to examine the
     * recent log activity without needing to access or alter persistent log files.
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    printLogsCache() {
        console.info(this.state.logCacheStream.logCache);
        return void 0 as unknown as this;
    }

    /**
     * Cleans up Microsoft Outlook-specific conditional comments and inline styles
     * from the loaded HTML document.
     *
     * This method performs two main clean-up operations on the HTML content to improve
     * email compatibility. It removes inline 'style' attributes that include Microsoft
     * Outlook-specific 'mso' prefixed styles, which can cause inconsistent rendering
     * in other email clients. Additionally, it removes HTML comments that utilise
     * conditional 'mso' directives intended solely for Microsoft Outlook.
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into state.$, an error is thrown.
     */
    cleanMsoConditions() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        $("*").each(function () {
            const style = $(this).attr("style");

            if (style && style.includes("mso")) {
                $(this).removeAttr("style");
            }
        });

        $("*")
            .contents()
            .filter(function () {
                return this.type === "comment" && /mso/i.test(this.data);
            })
            .remove();
        return void 0 as unknown as this;
    }

    /**
     * Removes Internet Explorer (IE) specific conditional comments from an HTML document. 
     *
     * IE conditional comments are often used to provide different CSS or JavaScript files to
     * IE browsers, usually to address compatibility issues. Many email clients may not interpret
     * causing undesired behaviours and making it render differently than expected. 
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    cleanConditionalComments() {
        let { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        // clean IE conditionals that surround the html tag
        const updatedHtml = $.html().replace(/<!--\[if\s[^<]*.*<!\[endif\]-->/gi, '');
        $ = loadWithCheerio(updatedHtml);

        // Function to check if a node is a conditional comment
        const isConditionalComment = (node: AnyNode) => {
            return node.type === 'comment' && /\[if\s.*\]>/.test(node.data);
        };

        // Filter out conditional comments
        $('*').contents().toArray().forEach(node => {
            if (isConditionalComment(node) && $) {
                $(node).remove();
            }
        });

        this.state.$ = $; // since we reassigned $, the reference can be lost, so reassign $ to the state
        return void 0 as unknown as this;
    }

    /**
     * Removes all HTML comment nodes from the loaded HTML document.
     *
     * This method traverses the loaded HTML content, identifying and removing any
     * comment nodes from the document's DOM structure. 
     * 
     * Each comment node is identified by its node type, which is '8' for comment nodes.
     * Upon identification, the method removes the comment node from its position in the
     * DOM tree, effectively cleaning the HTML of any comments that may be present.
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    cleanComments() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        $("*")
            .contents()
            .filter((index, node) => {
                // Node type 8 is a comment node
                return node.nodeType === 8;
            })
            .remove();
        return void 0 as unknown as this;
    }

    /**
     * Converts and embeds all external image sources within the HTML document to Base64-encoded data URIs.
     *
     * This asynchronous method searches through the HTML content for all `<img>` elements, retrieves
     * the images from their respective external source URLs, and replaces the `src` attribute of each
     * one with a Base64-encoded data URI representing the image content.
     *
     * During the processing of `<img>` tags, each external image is fetched asynchronously. If the image URL
     * is valid and the image can be successfully retrieved and encoded, it's replaced inline. If not, an error
     * is thrown that specifically notes a problem with encoding the remote image.
     * 
     * By default, each request's timeout is set to 30000 ms. If you have a big images or if you're on an unstable
     * or slow connection, you can increase the timeout by specifying `timeout` in the config when creating a 
     * Trogon instance. 
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {TrogonError} Thrown if there is a problem with fetching or encoding any of the external images.
     */
    // @ts-expect-error TS struggles with how the proxy resolves promises and allows for single await and then-free method chaining, so we assert that the async methods return this/proxy
    async embedExternalImgAsBase64(): this {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const imageElements = $("img");
        for (let i = 0; i < imageElements.length; i++) {
            const imgElement = $(imageElements[i]);
            const imageUrl = imgElement.attr("src");
            if (!imageUrl) continue;
            try {
                // Retrieve and convert each image to base64
                const base64Image = await Trogon.getImageBase64(imageUrl, this.config.timeout);
                // Determine the MIME type (assuming images are in a format compatible with common web browsers)
                const mimeType =
                    "image/" + path.extname(imageUrl).split(".").pop();
                // Replace the src attribute with the base64 string
                imgElement.attr(
                    "src",
                    `data:${mimeType};base64,${base64Image}`
                );
            } catch (err: any) {
                throw new TrogonError(
                    errorCodes.remoteImageEncodingProblem,
                    imageUrl,
                );
            }
        }
        return void 0 as unknown as this;
    }

    /**
     * Extracts the text content of the HTML <title> tag to use as the email subject.
     *
     * This method looks for the <title> tag within the loaded HTML content and assigns its text content
     * as the subject of the email, storing it in the instance's state for later usage. The method relies
     * on the Cheerio instance already being loaded with the HTML content  by calling `loadHTML`. 
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    inferSubjectFromTitle() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const title = $("title").text();
        this.state.subject = title;
        return void 0 as unknown as this;
    }

    /**
     * Assigns a user-defined subject to the instance's state for use in email construction.
     *
     * @param {string} subject The desired subject line text for the email. 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    setSubject(subject: string) {
        this.state.subject = subject;
        return void 0 as unknown as this;
    }

    /**
     * Prepares the email object with the subject and HTML body.
     * 
     * This method constructs an email-ready object, `state.mail`, which can be
     * directly utilised by a mail transport mechanism or written as an EML file.
     *
     * `state.mail` is initialised as an object compatible with the `Mail.Options`
     * type of NodeMailer.
     *
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    composeMail() {
        const { $, subject } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const html = $.html();
        this.state.mail = {
            subject,
            html,
            headers: {} as Mail.Headers,
        };
        return void 0 as unknown as this;
    }

    /**
     * Sets the X-Unsent header to 1.
     * 
     * The X-Unsent header in an EML file is typically used to indicate that the 
     * email should be opened in the email client as a draft, rather than as a 
     * received message.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `composeMail` hadn't been called before, an error is thrown.
     */
    markAsUnsent() {
        const { mail } = this.state;
        if (!mail) throw new Error(errorCodes.noComposedMail);
        // Assert that headers are not undefined since we set an empty object in composeMail()
        Object.assign(mail.headers!, { "X-Unsent": "1" });
        return void 0 as unknown as this;
    }

    /**
     * Assigns a user-defined domain name to be used for Message-ID generation.
     * 
     * This method provides the ability to specify a custom domain name that will be incorporated
     * into the Message-ID header of the email. It uses a regular expression pattern to validate
     * the user-provided `domain` against a shallow list of domain name criteria.
     * 
     * The validation covers several domain name aspects; it checks that domains do not start with
     * a hyphen, that each label (part of domain separated by '.') is of the correct length and
     * character composition, and that the top-level domain (TLD) is of an acceptable format.
     * 
     * @param {string} domain The custom domain name to validate and set for Message-ID headers.  
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {TrogonError} If user-submitted `domain` is not a valid domain name.
     */
    setDomain(domain: string) {
        const domainPattern = /^(?!-)(?:[a-zA-Z\d-]{0,62}[a-zA-Z\d]\.){1,126}(?!-)(?:[a-zA-Z\d]{1,63}\.?)+[a-zA-Z\d]{2,}$/;
        if (!domainPattern.test(domain)) throw new TrogonError(errorCodes.invalidDomainName, domain);

        this.state.domain = domain;
        return void 0 as unknown as this;
    }

    /**
     * Generates a Message-ID header for the mail message using specified domain.
     * 
     * According to email specifications (RFC 2822), every email should have a 
     * unique Message-ID value, which  is used to identify each message across 
     * email systems. 
     * 
     * For example, in Message-ID: <75581fbe-c6a6-3e1b-a268-cc3ac717ac00@localhost>, 
     * the unique string 75581fbe-c6a6-3e1b-a268-cc3ac717ac00 is typically a 
     * random UUID or similar value, and localhost indicates the domain name of 
     * the machine that generated it. This domain part should ideally be the 
     * sender's fully qualified domain name, but localhost is often used for emails 
     * generated on a local machine or when the generating system does not have a 
     * proper domain name configured. 
     * 
     * As such generating a Message-ID with a "good" domain name might be required
     * when dealing with older or otherwise jury-rigged email servers that may 
     * try to only allow a white-listed domain but won't regenerate the Message-ID
     * when you're sending the draft.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `setDomain` hadn't been called, an error is thrown.
     * @throws {Error} If `composeMail` hadn't been called before, an error is thrown.
     */
    generateMessageId() {
        const { mail, domain } = this.state;
        if (!mail) throw new Error(errorCodes.noComposedMail);
        if (!domain) throw new Error(errorCodes.noSetDomain);

        mail.messageId = `${randomUUID()}@${domain}`;
        return void 0 as unknown as this;
    }

    /**
     * Removes the Message-ID header from the output EML file.
     * 
     * Generally speaking you don't need to clean it, as most mailing clients
     * will overwrite it themselves. Certain mail servers may not do that and
     * may further attempt to block your email if the Message-ID doesn't feature
     * a white-listed domain name. 
     * 
     * When generating an EML file that is intended to be sent out as an email, 
     * you typically do not need to remove the Message-ID header. If you are 
     * using the MIME file as a message draft that users will then open and send 
     * through their email client, keeping the Message-ID is fine because the email
     * client will usually replace it with its own unique identifier when the email 
     * is actually sent. If you are sending the MIME file programmatically through 
     * an SMTP server or other mail-sending service, the server may also ignore your 
     * Message-ID and generate a new one or it may keep the one you have set if 
     * it's valid.However, if you have specific needs or instructions that require 
     * the Message-ID to be unset or regenerated at the time of sending, 
     * you may consider omitting it from the generated MIME file.
     * 
     * Since it updates the dumped EML file, this method doesn't work (or needed)
     * for sending emails programmatically.  
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    cleanMessageId() {
        fs.readFile(this.config.outputEmlPath!, 'utf-8', (err, data) => {
            if (err) throw new Error(errorCodes.noEmlFileFound);
            // Split the content into lines
            const lines = data.split(/\r?\n/);
            // Filter out lines that match the edge case criteria
            const filteredLines = lines.filter(line => !line.startsWith("Message-ID:"));
            // Re-join the remaining lines into a single string
            const updatedContent = filteredLines.join('\n');
            // Write the modified content back to the file
            fs.writeFile(this.config.outputEmlPath!, updatedContent, (err) => {
                if (err) throw new Error(errorCodes.emlWritingError);
            });
        });
        return void 0 as unknown as this;
    }

    /**
     * Generates a standard MIME structure (EML file) from the composed mail object
     * and writes it to the file system.
     * 
     * This asynchronous method is responsible for taking the mail data previously compiled
     * by calling `composeMail` method, converting it into a MIME message using the `MailComposer`
     * class provided by Nodemailer, and writing the resulting EML string to a file. The path
     * for the output file is taken from the config `outputEmlPath`.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `composeMail` hadn't been called before, an error is thrown.
     */
    // @ts-expect-error Assert return as "this" to help TS understand the proxy logic
    async dumpEml(): this {
        const { mail } = this.state;
        if (!mail) throw new Error(errorCodes.noComposedMail);

        const eml = await new MailComposer(mail).compile().build();
        fs.writeFileSync(this.config.outputEmlPath!, eml, "utf8");
        return void 0 as unknown as this;
    }

    /**
     * Configures the SMTP transport mechanism with the provided credentials and options
     * for sending emails programmatically through the Trogon instance.
     * 
     * This method initialises the SMTP transport using the given `transportOptions`, which
     * should conform to the `SMTPTransport.Options` interface from the Nodemailer library.
     * It creates the transport object using Nodemailer's `createTransport` function and stores
     * it in the instance's state, replacing any previously configured transport.
     *
     * After setting up the transport, this method also performs a convenience check on the
     * composed mail object within the instance's state. If a `from` address has not been
     * explicitly defined in the mail content, and the `transportOptions` contain an `auth.user`
     * property, the method sets the `from` address of the mail object to the `auth.user` value.
     * This ensures that outgoing emails have a sender address, which is especially useful if the
     * SMTP credentials include a default user and the sender is not separately specified during
     * the composing of the email.
     * 
     * @param {SMTPTransport.Options} transportOptions Nodemailer's SMTP transport.
     * configuration options, including credentials and other SMTP server settings.
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    setSmtpCred(transportOptions: SMTPTransport.Options) {
        this.state.smtpTransport = createTransport(transportOptions)

        // Default the sender if not specified and the SMTP creds contain a user
        const { mail } = this.state;
        if (mail && !mail.from && transportOptions.auth?.user) mail.from = transportOptions.auth?.user;

        return void 0 as unknown as this;
    }

    /**
     * Sets the sender for the composed mail object.
     *  
     * @param {string} from Sender's email. 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `composeMail` hadn't been called before, an error is thrown.
     */
    setSender(from: string) {
        const { mail } = this.state;
        if (!mail) throw new Error(errorCodes.noComposedMail);

        mail.from = from;
        return void 0 as unknown as this;
    }

    /**
     * Sets the recipients for the composed mail object.
     *  
     * @param {string | string[]} to Recipient's email or recipients' emails. 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If `composeMail` hadn't been called before, an error is thrown.
     */
    setRecipients(to: string | string[]) {
        const { mail } = this.state;
        if (!mail) throw new Error(errorCodes.noComposedMail);

        mail.to = to;
        return void 0 as unknown as this;
    }

    /**
     * Sends the composed email using the configured SMTP transport settings.
     *
     * Before attempting to send the email, this method performs a series of checks to ensure all
     * necessary configurations are in place. It validates the existence of an SMTP transport, a
     * composed email object, as well as the 'from' and 'to' fields within the mail object, which
     * are essential for sending an email. If any of these checks fail, it throws an error detailing
     * what configuration is missing, such as SMTP credentials, sender information, or recipient list.
     *
     * If all validations pass, it utilises the `smtpTransport` object to send the email with the
     * options and content specified in the composed `mail` object.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} Errors may be thrown to report missing SMTP configuration, email composition data,
     * a lack of sender or recipient information, or issues occurring during the sending process.
     */
    sendEmail() {
        const { smtpTransport, mail } = this.state;
        if (!smtpTransport) throw new Error(errorCodes.noSetSmtpCreds);
        if (!mail) throw new Error(errorCodes.noComposedMail);
        if (!mail.from) throw new Error(errorCodes.noSender);
        if (!mail.to) throw new Error(errorCodes.noRecipients);

        smtpTransport.sendMail(mail, (err) => {
            if (err) throw new Error(err?.message || errorCodes.cantSendEmail);
        });
        return void 0 as unknown as this;
    }

    /**
     * Copies the provided external stylesheet into the <style> tag in the head of the loaded HTML document.
     *
     * This method encapsulates the action of importing CSS content into the HTML document in such a way that
     * it becomes part of the document's <head> block. The CSS content is extracted using the `loadCss` method, 
     * which reads from a CSS file whose path is defined in the class's configuration.
     * 
     * Once loaded, the entire stylesheet content is wrapped in a <style> tag and prepended to the <head>
     * element of the HTML document. This process effectively inlines the external stylesheet, ensuring that
     * the styles are embedded directly within the HTML, which is particularly important for email templates
     * where external stylesheets are often not supported by email clients.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    convertCssToHeadStyleTag() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const styleSheet = this.loadCss();
        $('head').prepend(`<style>${styleSheet}</style>`);
        return void 0 as unknown as this;
    }

    /**
     * Transforms styles defined in internal stylesheets into inline styles within the HTML elements.
     * 
     * This method utilises the 'juice' library's functionality to take the current HTML document, with
     * its internal (within <style> tags) CSS styles, and apply those styles as inline 'style' attributes
     * on the respective HTML elements. This is especially beneficial for preparing HTML content for
     * email delivery, as inline styles are better supported by email clients compared to <style> tags
     * or external stylesheets.
     *
     * Upon successful inlining of the styles, the method re-initialises the state's Cheerio instance
     * with the updated HTML content, ensuring future operations will use the HTML with the now-inlined
     * styles.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     * @throws {Error} If the HTML content is not loaded into `state.$`, an error is thrown.
     */
    convertInternalStylesheetToInlineStyles() {
        const { $ } = this.state;
        if (!$) throw new Error(errorCodes.noHtmlLoaded);

        const updatedHtml = juice($.html());
        this.state.$ = loadWithCheerio(updatedHtml);

        return void 0 as unknown as this;
    }

    /**
     * Facilitates the conversion of an external CSS file to inline styles within the HTML content.
     *
     * This method first invokes `convertCssToHeadStyleTag` to transfer CSS from an external file 
     * into a `<style>` tag in the HTML document's `<head>`. Then, it calls `convertInternalStylesheetToInlineStyles` 
     * to convert these stylesheet styles to inline `style` attributes on individual HTML elements.
     *
     * Inline styles are much more reliably processed by email clients than styles 
     * located in `<style>` tags or external stylesheets. Inlining ensures that the email's styling 
     * appears as expected regardless of the email client's level of CSS support, delivering 
     * a consistent user experience.
     * 
     * @returns {this} The current `Trogon` instance, enabling method chaining to continue.
     */
    convertCssToInlineStyles() {
        this.convertCssToHeadStyleTag();
        this.convertInternalStylesheetToInlineStyles();
        return void 0 as unknown as this;
    }
}

export default Trogon;
export { Trogon };
