/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */
const errorCodes = {
    noSuchMethodInStateMachine: "noSuchMethodInStateMachine",
    missingRequirements: "missingRequirements", 
    remoteImageEncodingProblem: "remoteImageEncodingProblem",
    noHtmlLoaded: "noHtmlLoaded",
    noComposedMail: "noComposedMail",
    noSetDomain: "noSetDomain",
    invalidDomainName: "invalidDomainName",
    noEmlFileFound: "noEmlFileFound",
    noHtmlFileFound: "noHtmlFileFound",
    emlWritingError: "emlWritingError",
    noSetSmtpCreds: "noSetSmtpCreds",
    noSender: "noSender",
    noRecipients: "noRecipients",
    cantSendEmail: "cantSendEmail",
    noCssFileFound: "noCssFileFound",
    noSetLogsFile: "noSetLogsFile",
} as const;

export default errorCodes;