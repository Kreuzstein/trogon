/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */
import fs from "fs";
import { Writable } from "stream";

// Custom writable stream that caches log messages
class TrogonLogCacheStream extends Writable {
    private maxLogs: number;
    logCache: string[];

    constructor() {
      super({ objectMode: true });
      this.logCache = [];
      this.maxLogs = 1000; // Maximum number of log messages to store
    }
  
    _write(logRecord: string, _encoding: BufferEncoding, callback: (error?: Error | null) => void): void {
      if (this.logCache.length >= this.maxLogs) {
        this.logCache.shift(); // Discard the oldest log record if cache is full
      }
      this.logCache.push(logRecord);
      callback();
    }
  
    dumpCacheToFile(dumpFilePath: string) {
      const dumpStream = fs.createWriteStream(dumpFilePath, { flags: "w" });
      dumpStream.write("[");
      let onTheFirstItem = true;
      this.logCache.forEach(logRecord => {
        onTheFirstItem ? onTheFirstItem = false : dumpStream.write(",\n");  
        dumpStream.write(JSON.stringify(logRecord, null, 2));

      });
      dumpStream.write("]");
      dumpStream.end();
    }
  }

export default TrogonLogCacheStream;
