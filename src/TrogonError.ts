/*!
 * Copyright (c) 2023 Anton Kreuzstein <anton@quorum.ltd>
 * 
 * This file is part of Trogon.
 * 
 * Licensed under the Mozilla Public License Version 2.0. 
 * For full license terms, see LICENCE.md in the
 * root directory or http://mozilla.org/MPL/2.0/
 */

class TrogonError extends Error {
    code: string;
    data: any;

    constructor(code: string, data?: any) {
        super();
        this.message = code;
        this.code = code;
        this.data = data || undefined;
    }
}

export default TrogonError;
