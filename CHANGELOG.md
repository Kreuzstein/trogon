# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.2] - 2023-12-21

## Changed
- Updated README

## Removed
- Unused invalid config definition from unit tests

## [1.0.1] - 2023-12-17

### Added
- CHANGELOG.md
- Copyright notices

## Changed
- Updated README.md

## [1.0.0] - 2023-12-17

### Added
- Initial release

[unreleased]: https://gitlab.com/Kreuzstein/trogon/-/compare/v1.0.2...HEAD
[1.0.2]: https://gitlab.com/Kreuzstein/trogon/-/tags/v1.0.2
[1.0.1]: https://gitlab.com/Kreuzstein/trogon/-/tags/v1.0.1
[1.0.0]: https://gitlab.com/Kreuzstein/trogon/-/tags/v1.0.0